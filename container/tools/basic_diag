#!/bin/bash

# Function
  rebuild_nemo_tiles(){
     # Function to rebuild NEMO tiles, for files matching
     # the pattern in $pfx. The number of tiles is specified
     # as $ndomain. It is assumed the rebuild_nemo.exe is locally
     # available.

     fnpatt=${pfx}_0000.nc
     ndomain=`ls -l ${pfx}_[0-9]*.nc |wc -l`
     if [ -s "$fnpatt" ]; then
        # Create a namelist file for use with this rebuild
        rbnl_file=nam_rebuild
        echo "&nam_rebuild"           > $rbnl_file
        echo "    filebase='${pfx}'" >> $rbnl_file
        echo "    ndomain=$ndomain"  >> $rbnl_file
        echo "/"                     >> $rbnl_file

        # Do the rbld
        [ -s rebuild_nemo.exe ] && singularity exec -B /localscratch ${local_container_image} ./rebuild_nemo.exe || echo "rebuild_nemo.exe not available"
        [ -s ${pfx}.nc ] || echo "Failed to rebuild file ${pfx}.nc"
    
        # preserve time stamp from tiles
        touch -r $fnpatt ${pfx}.nc

        # remove individual tiles after rbld
        rm -f ${pfx}_[0-9][0-9][0-9][0-9].nc
      else
          echo "No files found for pattern $pfx"
      fi
  }

  # Do the rebuild
  start_date=${DIAG_YEAR}0101
  stop_date=${DIAG_YEAR}1231

  for sfx in 'grid_T' 'grid_U' 'grid_V' 'grid_W' 'icemod' 'diad_T' 'ptrc_T' 'grid_T_ar6' 'grid_U_ar6' 'grid_V_ar6' 'grid_W_ar6' ; do
      pfx=${RUNID}_1m_${start_date}_${stop_date}_$sfx
      rebuild_nemo_tiles 
  done

  rm -f ${RUNID}_1y*.nc  
  rm -f ${RUNID}_1d*.nc
  rm -f ${RUNID}_3h*.nc  

  nn_itend=$(cat time.step)
  end_step=$(echo $nn_itend | awk '{printf "%8.8d",$1}')

  # The physics rs file
  pfx=${RUNID}_${end_step}_restart

  fnpatt=${pfx}_0000.nc
  if [ -s "$fnpatt" ]; then
     rebuild_nemo_tiles
  fi
  
  pfx=${RUNID}_${end_step}_restart_ice
  fnpatt=${pfx}_0000.nc
  if [ -s "$fnpatt" ]; then
     rebuild_nemo_tiles
  fi
  
  pfx=${RUNID}_${end_step}_restart_trc
  fnpatt=${pfx}_0000.nc
  if [ -s "$fnpatt" ]; then
     rebuild_nemo_tiles
  fi

  pfx=mesh_mask
  rebuild_nemo_tiles
  mv mesh_mask.nc ${RUNID}_mesh_mask.nc  
  
  ###################################################################
  set +x
  module load nixpkgs/16.09  intel/2018.3  openmpi/3.1.4
  module load cdo/1.9.5
  set -x
  
  rebuild_canam_tiles(){
        mkdir -p $diag_month
        cd $diag_month

        num_tiles=32
        # Create a namelist file for use with this rebuild
        rbnl_file=candiag.nml
        echo "&candiag"                 > $rbnl_file
        echo "  file_prefix='${pfx}'"   >> $rbnl_file
        echo "  year=$DIAG_YEAR"        >> $rbnl_file
        echo "  month=$diag_month"      >> $rbnl_file
        echo "  num_tiles=$num_tiles"   >> $rbnl_file
        echo "  invariables='  ST', ' PCP', 'OUFS', 'OVFS', '  SV', '  SU', ' BEG', 'BEGI', 'BEGO', 'BWGO', 'CLDT', ' FLG', ' FSG', ' PSA', 'SICN', 'PHIS', 'LNSP', '  ES', 'TEMP', 'VORT', ' DIV' " >> $rbnl_file
        echo "/"                        >> $rbnl_file

        ln -s ../${pfx}${DIAG_YEAR}${diag_month}* .
        ln -s ../candiag_mwe.exe .
        ln -s ../ccc2nc_linux .
        ln -s ../temp_recipe .
        ln -s ../winds_recipe . 
        ln -s ${CANESM_SRC_ROOT}/CCCma_tools/scripts/subproc/ccc .

        echo "rebuild canam" $DIAG_YEAR $diag_month
        singularity exec -B /localscratch ${local_container_image} ./candiag_mwe.exe
        rm ${pfx}*
        rm candiag_mwe.exe

        # Compute from spectral fields with old diag
        ln -s ss npaksp
        singularity exec -B /localscratch -B /home -B /scratch --env BIN_DIR=$EXEC_STORAGE_DIR --env CANESM_SRC_ROOT=$CANESM_SRC_ROOT ${local_container_image} /bin/bash ./temp_recipe
        singularity exec -B /localscratch -B /home -B /scratch --env BIN_DIR=$EXEC_STORAGE_DIR --env CANESM_SRC_ROOT=$CANESM_SRC_ROOT ${local_container_image} /bin/bash ./winds_recipe
        cdo monmean gpt.nc ta_mon_${DIAG_YEAR}${diag_month}.nc
        cdo monmean gpv.nc va_mon_${DIAG_YEAR}${diag_month}.nc
        cdo monmean gpu.nc ua_mon_${DIAG_YEAR}${diag_month}.nc
        rm -f gpt.nc gpv.nc gpu.nc
        mv *_mon_*.nc ../
        cd ../
        rm -r $diag_month
        rm -f ${pfx}${DIAG_YEAR}${diag_month}_[0-9][0-9]*
  }

   pfx=OUTGCM
   for diag_month in '01' '02' '03' '04' '05' '06' '07' '08' '09' '10' '11' '12'; do
     rebuild_canam_tiles &
   done
   wait

   for var in `ls -1 *_mon*.nc | cut -f1 -d'_' | uniq`; do
       cdo mergetime ${var}*_mon*.nc ${var}_mon_${DIAG_YEAR}.nc
       #rm -f ${var}*_mon*.nc
   done    

   # Copy out the outputs
   mkdir -p ${OUTPUT_DIR}/processed
   cp ${RUNID}*.nc ${OUTPUT_DIR}/processed
   cp *_${DIAG_YEAR}.nc ${OUTPUT_DIR}/processed
       



