#!/bin/bash
#PBS -S /bin/bash
#PBS -q development
#PBS -j oe
#PBS -l walltime=05:00:00
#PBS -l select=1:ncpus=40:mem=50gb:res_image=eccc/eccc_all_ppp_ubuntu-18.04-amd64_latest
set -e
ncpus=40

#================================
# Source config file for this run
#================================
source ${WRK_DIR}/canesm.cfg

#====================================
# Get run years from experiment table
#====================================
# Determine index of Run start:stop year column
START_STOP_HEADING="NC start:stop year"

# -- Turn headings into bash array, loop through it, looking for $START_STOP_HEADING
IFS='|' read -ra HEADINGS <<< "$(head -n 1 $ncconv_exptab)"
for i in "${!HEADINGS[@]}"; do
    # trim leading and trailing white space 
    HEADING=$( echo ${HEADINGS[$i]} | awk '{$1=$1;print}')

    # store index and break from loop if found
    if [[ $HEADING == $START_STOP_HEADING ]]; then
        START_STOP_IND=$i
        break
    fi
done

# extract entry from experiment table and populate into array
# -- Pattern ensures to only match rows with the FULL runid in the first column
RUNID_ENTRY=$(cat $ncconv_exptab | grep "^\s*|\s*${runid}\b")
IFS='|' read -ra RUNID_ENTRY_ARRAY <<< "$RUNID_ENTRY"

# export start_time and stop_time into the environment to be used by
# ncat.var after removing leading/trailing whitespace with 'awk'
START_STOP_ENTRY=${RUNID_ENTRY_ARRAY[$START_STOP_IND]}
start_time=${START_STOP_ENTRY%%:*}
stop_time=${START_STOP_ENTRY##*:}
export start_time=$( echo $start_time| awk '{$1=$1;print}' )
export stop_time=$( echo $stop_time | awk '{$1=$1;print}' )

#========================================
# Determine list of tables to concatenate
#========================================
# get cimp6 directory structure down to variant number
cd $ncout
dircmip6=${ncout}/$(find CMIP6 -maxdepth 5 -mindepth 5 -type d)

# get cmor table list in the directory
cd $dircmip6
[[ $ctablist ]] || ctablist=$(ls )

# remove some tables from the concatenation list
#   - fixed, and climatology tables are ignored for obvious reasons
#   - subdaily frequencies are not functional at the moment
do_not_concat_list="Ofx fx Oclim CFsubhr 6hrLev"
for ctab in $do_not_concat_list ; do
    ctablist=$(echo ${ctablist} | sed "s/${ctab}//")
done

#===================================================================================================
# Loop over all (desired) cmor tables, putting the concatenation for each variable in the background
#===================================================================================================
# limit the number of queued/running background processes to 2*ncpus
proc_limit=$((2 * ncpus))
n_procs=0
for ctab in $ctablist; do
    cd $dircmip6/$ctab
    # get variable list in the directory
    varslist=$(ls )

    # loop over all variables and do the concatenation
    for var in $varslist ; do
        (${CCRNSRC}/ncconv/bin/ncat.var $runid $ctab $var $ncout)&

        # increment process count, waiting if we've reached the limit
        n_procs=$((n_procs + 1))
        if (( n_procs == proc_limit )); then
            wait
            n_procs=0
        fi
    done
done

# Wait for all background processes to finish
wait 
echo "Concatenation done!"
