"""
Module with routines that should be removed after CMIP6 is done. This is generally
used to fix bugs that were fixed during CMIP6, but were only detected after a
significant number of runs were completed
"""

import os
import subprocess

def detect_nc_or_not( filename ):
    """
    A bug in the offline BGC deck for NEMO led to some of the variables calculated
    offline not having a .nc extension. This function first checks to see if the
    '.nc' file exists, if not, then it checks for the filename without the extension.
    If that also doesn't exist, then it simply returns the original filename (which
    should then fail out like normal).
    
    Input:
        filename (str) : the original filename to check. Should have a .nc extension
    """
    temp_access_name = 'temp_access_file'
    subprocess.call(['access','temp_access_name',filename])
    if os.path.exists('temp_access_name'):
        os.remove('temp_access_name')
        return filename
    else:
        subprocess.call(['access','temp_access_name',filename+'.nc'])
        if os.path.exists('temp_access_name'):
            os.remove('temp_access_name')
            return filename+'.nc'
    return filename
