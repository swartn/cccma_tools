#!/usr/bin/env python
""" Driver program for converting CCCma data to CMORized NetCDF
    
    Parses command line args, and call table_utils / nmor to do 
    netCDF conversion.

    Use python pycmor.py -h for more help.
"""
import json
import argparse
import traceback
import sys
import resource
import table_utils as tu
import answer_checks as ac

# Add command line arguements
parser = argparse.ArgumentParser()
parser.add_argument("-r", "--runid",    type=str,      
                        help="The runid to CMORize")
parser.add_argument("-u", "--user",     type=str,
                        help=("What user ID was used to run the simulation. Used to determine model hash." +
                              " Defaults to the pycmor user."), default=None)
parser.add_argument("-t", "--ctables",  type=str,    
                        help="A whitespace separated list of the CMOR table(s) to process (e.g. Omon)")
parser.add_argument("-c", "--cmorvars", type=str, 
                        help=("A whitespace separated list of the CMOR variables to process (e.g. 'tos thetao'). "+
                              "If used with '-t', each var is searched for in each table, and a warning is "+
                              "printed if the var isn't found in at least ONE table")) 
parser.add_argument("-p", "--project",  type=str,
                        help="Project name (used to identify json tables containing conversion info for the project and model version)")
parser.add_argument("-k", "--chunk",    type=str, 
                        help="Time chunk, yyyy_yyyy or yyyymm_yyyymm format")
parser.add_argument("-m", "--nemo_mesh_file", type=str, 
                        help="NEMO mesh mask file - defaults to sc_{runid}_{chunk}_mesh_mask.nc")
parser.add_argument("-L", "--cmorvarlogs", 
                        help="Activates individual variable logs for CMOR output",
                        dest='use_cmorvar_logs', action='store_true', default=False)
parser.add_argument("-o", "--outpath", type=str, default='output',
                        help="Output dir for CMORized netcdf files (if given, overrides 'outpath' in cccma_user_input.json)")
parser.add_argument("-v", "--default_version", type=str, default='default_version',
                        help="Final component of output path, indicating the version of the data")
parser.add_argument("-n", "--new_version", type=str, default='',
                        help="Alternate version string to use for retracted variables")
parser.add_argument("--exptable", type=str, default='cmip6_experiments/cmip6_experiment.txt',
                        help="path to experiment table (Defaults to cmip6_experiments/cmip6_experiment.txt)")
parser.add_argument("--prod", action='store_true', default=False, 
                        help=("make pycmor run conversion if 'production' mode. As of 2019-03-04, this only controls the presence "+
                              " a disclaimer in the resulting meta data."))
parser.add_argument('--test_compliance', action='store_true', default=False, 
                        help=('make pycmor test each successfully converted file for ESGF compliance. ' +
                              'Note: This significantly slows down the conversion process if used'))
parser.add_argument('--check_deltas', action='store_true', default=False,
                        help=('calculate array checksums for the converted files and compare them to those stored in '+
                              'bin/pycmor/continuous_integration/nightly_builds, throwing an error if a published vars hash has changed. '+
                              'Regardless if hashes change or not, using this flag creates a var_hashes.json file in the '+
                              'working directory. See --append_hashes if you wish to append to this file instead of creating '+
                              'a new one.'))
parser.add_argument('--update_hashes', action='store_true', default=False,
                        help=('if --check_deltas is used in conjunction with this flag, when writing the var_hashes.json file '+
                              'pycmor will update it, instead of creating a clean one.'))
parser.add_argument('--no_exclusions', action='store_true', default=False,
                        help=('make pycmor ignore ALL exclusion flags for this conversion, attempting to convert all the data it can.'+
                              ' This is useful when wanting to test full conversion capabilities.'))
args = parser.parse_args()

# check flags
if args.update_hashes and not args.check_deltas:
    print("Warning: '--update_hashes' must be used in conjunction with '--check_deltas'. Ignoring flag...")
if args.no_exclusions:
    print("Ignoring all exclusion clauses in variable/experiment tables")
if args.prod:
    print("Producion mode on! Checking for uncommitted changes in the working repo.")
    tu.check_repo()

# Split the space-separated cmor table and variable input strings into lists
ctable_list = args.ctables.split(' ')
cvars_list = args.cmorvars.split(' ') if args.cmorvars else None

# Set model hash for this conversion
print("Setting model hash")
model_hash = tu.get_model_hash(args.runid, run_user=args.user, production=args.prod)

# For each cmor table, parse the associated CCCma variable table and call the conversion scripts.
print("Beginning conversions on {}".format(" ".join(ctable_list)))
for tab in ctable_list:
    print("Processing CMOR table: {}".format(tab))
    try: 
        tu.convert_table(runid=args.runid, cmor_table=tab, project=args.project, chunk=args.chunk, cmorvars=cvars_list, model_hash=model_hash, 
                            nemo_mesh_mask_file= args.nemo_mesh_file, use_cmorvar_logfile=args.use_cmorvar_logs, 
                            check_deltas=args.check_deltas, test_compliance=args.test_compliance, production=args.prod,
                            outpath=args.outpath, default_version=args.default_version, new_version=args.new_version,
                            no_exclusions=args.no_exclusions,exptable=args.exptable)
        print("Completed CMOR table: {}".format(tab))
    except Exception as exc:
        print("Exception occured when converting variable table {} \n Error output:".format(tab)) 
        print(traceback.format_exc())
        print("skipping {}".format(tab))
        continue
    
    # output memory information
    pycmor_mem_usage = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    subprc_mem_usage = resource.getrusage(resource.RUSAGE_CHILDREN).ru_maxrss
    print("\nCurrent pycmor RAM usage, after table {} = {} kb".format(tab,pycmor_mem_usage))
    print("Current subproc RAM usage, after table {} = {} kb\n".format(tab,subprc_mem_usage))

# if user gave a list of variables, check that they were all found in at least one of the requested CMOR tables
if cvars_list:
    for var in cvars_list:
        if var not in tu.user_req_vars_in_tables:
            print("\nWarning.. user requested var '{}' not found in any of the requested tables ({})".format(var," ".join(ctable_list)))

# dump calculated variable hash to json file
if args.check_deltas:
    ac.write_answers(tu.var_hashes, update_hashes=args.update_hashes)
