#!/usr/bin/env python
"""
    This module contains code that will be used to test for compliance with ESGF standards.
    The functions here can be imported or it can be called from the command line. In order to see
    the command line arguments, run "python compliance_checks.py -h"

    Classes:
        CMORtableError

    Fnctns:
        verify_CMORtab
        parse_PrePARE
        parse_cfcheck
        test_compliance

"""
import argparse
import os 
import subprocess 
import re 
import sys
import table_utils as tu
from subprocess import PIPE


class CMORtableError(Exception):
    """
        Error class that will be raised when inconsistencies are found between 
        CCCma CMOR tables and the true CMOR tables.
    """
    pass

def verify_CMORtab(tab_dat, CMOR_table, project, cmortab_dir='cmip6-cmor-tables/Tables', full_check=False):
    """
        Take in dictionary and compare to information in "true" CMOR tables, checking for 
        consistency and throwing an exception if not. By default, it just checks that the 
        variables in 'tab_dat' are contained in the true CMOR tables, but if full_check is 
        set to true, it checks that all fields for each variable found in the true tables
        are found and match those in tab_dat.
        
        Inputs: 
            tab_dat             (dict)  : dictionary containing data to be verified
            CMOR_table          (str)   : name of CMOR table to check (ex: Omon)
            project             (str)   : name of project to which CMOR tables belong (e.g. "CMIP6")
            cmortab_dir         (str)   : directory containing the true CMOR tables
            full_check          (bool)  : set to true for comprehensive check of variable fields
                                            Note: at the moment, setting this to True halts ALL conversions, due to
                                                    mismatches in 'positive', 'ok_min_mean_abs', and 'comment'
    """
    
    # get true table values
    true_CMOR_tab        = tu.get_vartab(cmortab_dir,CMOR_table,project,filename_template='{project}_{table}')
    true_CMOR_tab_dat    = true_CMOR_tab['variable_entry']

    # loop through tab_dat and compare values against true_CMOR_tab_dat
    for var,row in tab_dat.iteritems():
        assert var == row['CMOR Name']

        # get "true" variable data
        try:
            true_CMOR_tab_var = true_CMOR_tab_dat[var]
        except KeyError as e:
            raise CMORtableError("{} not found in {} CMOR table {}!".format(var,project,CMOR_table))

        # if desired check that all fields match
        if full_check:
            for key,true_CMOR_val in true_CMOR_tab_var.iteritems():
                try:
                    if not row[key] == true_CMOR_val:
                        err_txt = "{}: CCCma attribute {} does not match the value in the {} {} table!".format(var,key,project,CMOR_table)
                        raise CMORtableError(err_txt)
                except KeyError as e:
                    err_txt = "{}: {} attribute {} not found in CCCma {} table!".format(var,project,key,CMOR_table) 
                    raise CMORtableError(err_txt)

def parse_PrePARE(output):
    """
        Take in output from PrePARE and parse it, returning the stats, i.e. how many files were scanned,
        or how many resulted in errors.
        
        Inputs:
            output  (str)   : output from PrePARE

        Ouputs:
            stats   (dict)  : dictionary containing the number of files scanned and files with errors
    """
    # initiate dict object
    stats = {}

    # define stat line identifiers
    num_files_str   = 'Number of files scanned:'
    num_errs_str    = 'Number of file with error(s):'

    # define regex color code identifier
    rm_clrcds = re.compile(r'\x1b.*')
    
    # loop through output lines and grab stats from the stats lines
    output_lines = output.split('\n')
    for line in output_lines:
        if num_files_str in line:
            num_files = line.split()[-1]
        elif num_errs_str in line:
            num_errs = line.split()[-1]

    # convert to integer values after removing color codes
    num_files   = int(rm_clrcds.sub("",num_files))
    num_errs    = int(rm_clrcds.sub("",num_errs))

    # store stats and return 
    stats['files_scanned']  = num_files
    stats['files_w_errs']   = num_errs
    return stats

def parse_cfcheck(output):
    """
        Take in output from cfchecks and parse it, returning the stats on errors, 
        warnings and information messages.

        Inputs:
            output  (str)   : output from cfcheck

        Outputs:
            stats   (dict)  : dictionary containing stats that were ouput from cfcheck, specifically
                                how many error, warning, and information messages were thrown.
    """
    # initiate dict object
    stats = {}

    # define stat line identifiers
    num_errs_str    = 'ERRORS detected:'
    num_wrns_str    = 'WARNINGS given:'
    inf_msg_str     = 'INFORMATION messages:'

    # loop through output lines and grab stats
    output_lines = output.split('\n')
    for line in output_lines:
        if num_errs_str in line:
            num_errs = line.split()[-1]
        elif num_wrns_str in line:
            num_wrns = line.split()[-1]
        elif inf_msg_str in line:
            num_inf_msg = line.split()[-1]

    num_errs    = int(num_errs)
    num_wrns    = int(num_wrns)
    num_inf_msg = int(num_inf_msg)
    stats['Errors_detected']    = num_errs
    stats['Warnings_given']     = num_wrns
    stats['Information_msgs']   = num_inf_msg
    return stats

def test_compliance(fls, log_f, cmortab_dir='cmip6-cmor-tables/Tables'):
    """
        Run the given list of files through the (at the moment, predicted) 'compliance pipeline',
        and confirm if it is publishable or not.

        Notes: 
            - at the moment we parse the output to figure out what happened, but its possible there 
                is a better/cleaner way.. maybe via return codes?
            - sometimes an unknown error occurs internal to cfcheck which causes the output to be thrown
              off, which makes the 'parse_cfcheck()' function fail. It seems that the error is related to 
              an internet connection problem, which makes cfcheck fail to find the tables it needs for its
              compliance guidelines. We don't currently know how to properly fix this, so we just recommend
              re-running this function, which often fixes the problem.

        Inputs: 
            fls                     (str/list of str)   : list of files to check
            log_f                   (str)               : file to store the output
            cmortab_dir             (str)               : location of the TRUE cmor tables
    """
    # define static strings
    file_seperator  = "\n***************************************************************************\n"
    PrePARE_head    = "PrePARE\n++++++++++\n"
    CFchck_head     = "\n\nCFchecker\n++++++++++++++\n"
    stdout_head     = "......................\n        stdout        \n......................\n"
    stderr_head     = "......................\n        stderr        \n......................\n"

    # init stats dictionary
    stats = {
                'PrePARE_errs' : 
                    { 'files' : [] },
                'cfcheck_errs' :
                    { 'files' : [] },
                'cfcheck_wrns' :
                    { 'files' : [] },
            }

    # check if a single file or list of files was given
    # and create single element list if so
    if isinstance(fls,str):
        fls = [fls]

    for fl in fls:
        # PrePARE
        cmd = ['PrePARE', '--table-path', cmortab_dir, fl]
        PrePARE_proc = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        (PrePARE_stdout,PrePARE_stderr) = PrePARE_proc.communicate()
        PrePARE_rc                      = PrePARE_proc.returncode

        # cf-checker
        cmd = ['cfchecks',fl]
        CFchck_proc = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        (CFchck_stdout,CFchck_stderr)   = CFchck_proc.communicate()
        CFchck_rc                       = CFchck_proc.returncode

        # check/store information on stats
        PrePARE_stats   = parse_PrePARE(PrePARE_stdout)
        try:
        # see notes in function doc string
            CFchck_stats = parse_cfcheck(CFchck_stdout)
        except Exception as e:
            err_string = "cfchecks failed on {}\n".format(fl)
            err_string += "Unexpected output experienced.. please rerun the compliance test\n"
            raise Exception(err_string)            
            
        if PrePARE_stats['files_w_errs'] > 0:
            stats['PrePARE_errs']['files'].append(fl)
        if CFchck_stats['Errors_detected'] > 0:
            stats['cfcheck_errs']['files'].append(fl)
        if CFchck_stats['Warnings_given'] > 0:
            stats['cfcheck_wrns']['files'].append(fl)

        # write output to log_f
        with open(log_f,'a') as f:
            # write seperator 
            f.write(file_seperator)
            f.write(fl)
            f.write(file_seperator)

            # write PrePARE output
            f.write(PrePARE_head)
            f.write("Return code: {}\n".format(PrePARE_rc))
            f.write(stdout_head)
            f.write(PrePARE_stdout)
            f.write(stderr_head)
            f.write(PrePARE_stderr)

            # write CF checker output
            f.write(CFchck_head)
            f.write("Return code: {}\n".format(CFchck_rc))
            f.write(stdout_head)
            f.write(CFchck_stdout)
            f.write(stderr_head)
            f.write(CFchck_stderr)
            f.write("\n")

    # count number of files and return dictionary
    stats['PrePARE_errs']['nfiles'] = len(stats['PrePARE_errs']['files'])
    stats['cfcheck_errs']['nfiles'] = len(stats['cfcheck_errs']['files'])
    stats['cfcheck_wrns']['nfiles'] = len(stats['cfcheck_wrns']['files'])
    return stats

#==============================
# If ran from the command line
# =============================
if __name__ == '__main__':
    
    #===========================
    # define command line parser
    #===========================
    parser = argparse.ArgumentParser(description="Check ESGF compliance of netcdf files.")
    
    #-- create mutually exclusive group, it either wants a list of files, or a directory
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-f", "--files", metavar="FILES", nargs="+", 
                        help="space delimited list of files to check for ESGF compliance")
    group.add_argument("-d", "--directory", metavar="DIRECTORY", 
                        help="directory containing files to check for ESGF compliance")

    #-- optional arguments
    parser.add_argument("--logfile", metavar="LOGFILE", default='compliance.log', 
                        help="name of log file to store PrePARE and cfcheck output. Defaults to 'compliance.log'")
    parser.add_argument("--table_path", metavar="PATH2_CMOR_TABLES", default='cmip6-cmor-tables/Tables',
                        help="path to cmor tables. Defaults to 'cmip6-cmor-tables/Tables'")
    parser.add_argument("--list_wrns", action="store_true", default=False,
                        help="make compliance_checks.py output the list of files causing cfcheck warnings")
    parser.add_argument("--list_errs", action="store_true", default=False,
                        help="make compliance_checks.py output the list of files causing errors (both from PrePARE and cfcheck)")

    #===================
    # Parse CL arguments 
    #===================
    args = parser.parse_args()
    logfile     = args.logfile
    table_path  = args.table_path
    if args.files:
        # use list of files provided by user
        files = args.files
    elif args.directory:
        # recursively find netcdf files within specified dir
        directory = args.directory
        files = []
        # get filenames
        for d,_,filenames in os.walk(directory):
            for filename in filenames:
                if filename.endswith('.nc'):
                    # only consider netcdf files
                    files.append(os.path.join(d,filename))
    # clean old log file if one exists
    if os.path.isfile(logfile):
        print("Warning: cleaning old logfile {}".format(logfile))
        os.remove(logfile)
    
    #====================
    # Test for compliance 
    #====================
    compliance_stats = test_compliance(files,logfile,table_path)

    #==============
    # Write summary
    #==============
    summary  = "\nFiles Inspected:                {}\n".format(len(files))
    summary += "Files with PrePARE errors:      {}\n".format(compliance_stats['PrePARE_errs']['nfiles'])
    summary += "Files with cfcheck errors:      {}\n".format(compliance_stats['cfcheck_errs']['nfiles'])
    summary += "Files with cfcheck warnings:    {}\n".format(compliance_stats['cfcheck_wrns']['nfiles'])
    if args.list_wrns:
        # only try to output list if any files actually caused cfcheck warnings
        if compliance_stats['cfcheck_wrns']['files']:
            summary += "\nFiles causing cfcheck warnings:\n"
            for f in compliance_stats['cfcheck_wrns']['files']:
                summary += "\t{}\n".format(f)
        else:
            summary += "\nNo cfcheck warnings!\n"
    if args.list_errs:
        # only try to output list if any files actually caused errors
        if compliance_stats['cfcheck_errs']['files'] or compliance_stats['PrePARE_errs']['files']:
            summary += "\nFiles causing errors:\n"
            if compliance_stats['cfcheck_errs']['files']:
                summary += "\tcfcheck:\n"
                for f in compliance_stats['cfcheck_errs']['files']:
                    summary += "\t\t{}\n".format(f)
            if compliance_stats['PrePARE_errs']['files']:
                summary += "\tPrePARE:\n"
                for f in compliance_stats['PrePARE_errs']['files']:
                    summary += "\t\t{}\n".format(f)
        else:
            summary += "\nNo errors!\n"
    summary += "\nSee {} for specifics on warnings/errors\n".format(logfile)
    print(summary)

