from fabric import Connection
import os
from typing import Dict, Sequence, Union
import appdirs
import sqlite3
import uuid
from filelock import FileLock
import numpy as np
import collections.abc

def log_directory():
    try:
        os.makedirs(appdirs.user_data_dir('canesm-ensemble'))
    except OSError:
        pass
    return appdirs.user_data_dir('canesm-ensemble')

def update(d,u):
   """
   Recursively update a dictionary, d, with an update, u.

   If an item appears in only one dictionary, it is included in the result.

   Inputs:
     d : dict to update
     u : dict of updates
   Returns
     d : updated dict

   Based on:
   https://stackoverflow.com/questions/3232943/update-value-of-a-nested-dictionary-of-varying-depth
   """
   for k, v in u.items():
      if isinstance(v, collections.abc.Mapping):
         d[k] = update(d.get(k, {}), v)
      else:
         d[k] = v
   return d

class RemoteFile:
    """
    Copy a remote file from the server, open it for changes, and upon exiting copy it back to the server and
    remove the local copy
    """
    def __init__(self, filename: str, machine: str, user: str, mode: str = 'r'):
        self.filename = filename
        self.tmp_filename = os.path.join(log_directory(), str(uuid.uuid4()) + '.tmp')
        self.conn = Connection(machine, user)
        self.mode = mode
        self.machine = machine
        self.user = user

    def __enter__(self):
        with Connection(self.machine, self.user) as c:
            c.get(self.filename, self.tmp_filename)
        self.open_file = open(self.tmp_filename, self.mode)
        return self.open_file

    def __exit__(self, *args):
        self.open_file.close()
        if self.mode != 'r':
            with Connection(self.machine, self.user) as c:
                c.put(self.tmp_filename, self.filename)
        os.remove(self.tmp_filename)


class RemoteDBConn:
    """
    Helper for sqlite3 connections to databases on remote machines. Given a remote connection
    copy the database file to the local machine, and open a connection. On exit close the connection
    put the file back on remote and get rid of the local copy. While active the database file is locked
    to avoid syncing issues, but this won't be perfect.

    Parameters
    ----------
    filename:
        name of database file
    machine:
        name of remote machine
    user:
        account on remote machine used for ssh connection

    Examples
    --------

    >>> with RemoteDBConn(db_file, machine, user) as db_conn:
    >>>     c = db_conn.cursor()
    >>>     c.execute('select * from table')
    >>>     result = c.fetchall()

    .. note:: sqlite3 is not designed for remote
       connections so this is a bit of a hack until a proper database is implemented
    """
    def __init__(self, filename: str, machine: str, user: str):
        self.filename = filename
        self.tmp_filename = os.path.join(log_directory(), os.path.basename(self.filename) + '.tmp')
        self.lock = FileLock(os.path.join(self.tmp_filename + '.lock'), timeout=10)
        self.machine = machine
        self.user = user

    def __enter__(self):
        self.lock.acquire(timeout=10)
        try:
            with Connection(self.machine, self.user) as c:
                c.get(self.filename, self.tmp_filename)
        except FileNotFoundError:
            pass
        self.db_conn = sqlite3.connect(self.tmp_filename)
        return self.db_conn

    def __exit__(self, *args):
        self.db_conn.close()
        with Connection(self.machine, self.user) as c:
            c.put(self.tmp_filename, self.filename)
        os.remove(self.tmp_filename)
        self.lock.release()

class FileProcessor:
    """Parent class to process namelist or bash files with helper functions"""

    def process(self, settings: Dict):
        raise NotImplementedError

    def is_blank_or_comment(self, line: str, delimiter: str) -> bool:
        return len(line.strip()) == 0 or line.strip()[0] == delimiter

    def remove_whitespace(self, line: str) -> str:
        return line.replace(" ", "")

    def is_assigned(self, key: str, line: str) -> str:
        return f"{key}=" in self.remove_whitespace(line)

    def process_variable(self, line, key, val):
        return line.replace(key, str(val))

    def check_missing_keys(self, settings: Dict, found_keys: Dict):
        missing_keys = set(settings) - set(found_keys)
        if missing_keys:
            raise ValueError(f"option: {missing_keys} was not found")

    def separate_comments(self, line: str, delimiter: str) -> dict:

        line_no_comment = line.split(delimiter)[0] if delimiter in line else line
        comment = f" {delimiter}{line.split(delimiter)[1]}" if delimiter in line else ""
        line_no_comment = self.remove_whitespace(line_no_comment)

        return line_no_comment, comment


class ProcessNamelist(FileProcessor):
    """
    Process a namelist file, replacing key=val
    """

    def __init__(self, file_contents):
        self.file_contents = file_contents

    def process_namelist_line(self, line, key, val):
        line_no_comments, comment = self.separate_comments(line, delimiter="!")
        if self.is_assigned(key, line_no_comments):
            line = 6*" "+f"{key}={str(val)}{comment}"

        return line

    def process(self, settings):
        lines = []
        found_keys = {}

        for line in self.file_contents.split("\n"):
            if self.is_blank_or_comment(line, delimiter="!"):
                pass
            else:
                for key in settings:
                    if self.is_assigned(key, line):
                        line = self.process_namelist_line(line, key, settings[key])
                        found_keys[key] = True
            lines.append(line)  # keep line regardless

        self.check_missing_keys(settings, found_keys)

        return ProcessNamelist("\n".join(lines)).file_contents


class ProcessBash(FileProcessor):
    """
    Process a bash file, replacing option=val
    """

    def __init__(self, file_contents):
        self.file_contents = file_contents

    def is_expanded_variable(self, key: str, line: str) -> str:
        return key[0] == "$" and key in line

    def process_bash_line(self, line: str, key: str, val: str) -> str:

        line_no_comments, comment = self.separate_comments(line, delimiter="#")
        multi_variable_line = (
            ";" in line_no_comments and line_no_comments.count("=") > 1
        )

        # ignore expanded variables
        if (
            line_no_comments.split("=")[0] != key and not multi_variable_line
        ):  # reject new_key=val
            pass
        else:
            if (
                multi_variable_line
            ):  # if its a line with multiple variables split it and process each one
                line = (
                    f"{self.process_multivarline(line_no_comments, key, val)}{comment}"
                )
            else:
                line = f"{line_no_comments.split('=')[0]}={str(val)}{comment}"  # keeps comment intact

        return line

    def process_multivarline(self, line: str, key: str, val: str) -> str:
        """Replaces values with variables that are set on a line
        with more than one variable assignment
        """
        separated_vars = self.remove_whitespace(line).split(";")
        (
            separated_vars.remove("") if "" in separated_vars else None
        )  # remove list element with ''
        for idx, variable in enumerate(separated_vars):
            var_key = variable.split("=")[0]

            if var_key == key:
                variable = f"{var_key}={str(val)}"
            separated_vars[idx] = variable

        separated_vars = [
            f"{var.split('=')[0]}={var.split('=')[1]}" for var in separated_vars
        ]
        return ";".join(separated_vars)

    def process(self, settings: dict):
        # TODO: allow for variables across multiple lines (file_lists)
        # TODO: there must be a cleaner way than looping over all the keys and lines
        lines = []
        found_keys = {}

        for line in self.file_contents.split("\n"):
            if self.is_blank_or_comment(line, delimiter="#"):
                pass
            else:
                for key in settings:
                    if self.is_assigned(key, line):
                        line = self.process_bash_line(line, key, settings[key])
                        found_keys[key] = True
                    elif self.is_expanded_variable(key, line):
                        line = self.process_variable(line, key, settings[key])
                        found_keys[key] = True
            lines.append(line)  # keep line regardless of it's status

        self.check_missing_keys(settings, found_keys)

        return ProcessBash("\n".join(lines)).file_contents


def divide_list(l: Sequence, n: int):
    """
    Divide a list into chunks of size n

    Parameters
    ----------
    l :
        input list
    n :
        chunk size

    Yields
    ------
        n elements of the list
    """
    for i in range(0, len(l), n):
        yield l[i:i + n]


def year_from_time(time) -> int:
    """
    Get the year in the time field

    Parameters
    ----------
    time :
        A time in either integer or str (YYYY_mMM) format

    Returns
    -------
        year
    """

    year = int(convert_date(time).split('_m')[0])
    return year


def month_from_time(time, default_month: int = 12) -> int:
    """
    Get the month in the time field

    Parameters
    ----------
    time :
        A time in either integer or str (YYYY_mMM) format
    default_month:
        If month is not specified assume this month

    Returns
    -------
        month
    """
    month = int(convert_date(time, default_month=default_month).split('_m')[1])
    return month


def convert_date(date, default_month=12) -> str:
    """
    Convert the date to the canesm version 'YYYY_mMM'

    Raises
    ------
    ValueError
        If the string cannot be converted
    """
    if type(date) is str:
        try:
            date = f'{int(date):04d}_m{default_month:02}'
        except ValueError:
            for mdelim in ['_m', 'm', ':', '-']:
                if mdelim in date:
                    break
            else:
                raise ValueError('date format not recognized')
            year = int(date.split(mdelim)[0])  # check that year and month are integers
            month = int(date.split(mdelim)[1])
            date = f'{year:04d}_m{month:02d}'
    elif type(date) is int:
        date = f'{int(date):04d}_m{default_month:02d}'
    elif type(date) is float:
        if abs(int(date) - date) > 0.001:
            raise ValueError('could not interpret float as date format, try YYYY_mMM')
        date = f'{int(date):04d}_m{default_month:02d}'
    else:
        raise ValueError('could not interpret date format, try YYYY_mMM')

    return date


def previous_month(date: str = None, year: int = None, month: int = None) -> str:

    if date is not None:
        date = convert_date(date)
        year = year_from_time(date)
        month = month_from_time(date)

    if month == 1:
        return f'{year-1}_m12'
    else:
        return f'{year}_m{month - 1:02d}'


def add_time(date: str = None, delta: Union[str, int] = None, year: int = None, month: int = None) -> str:

    if date is not None:
        date = convert_date(date)
        year = year_from_time(date)
        month = month_from_time(date)

    delta = convert_date(delta, default_month=0)
    delta_years = int(delta.split('_m')[0])
    delta_months = int(delta.split('_m')[1])

    if delta_months == 0:
        new_date = f'{year + delta_years}_m{month:02d}'
    else:
        add_years = delta_years + ((month + delta_months) // 12)
        add_month = (month + delta_months) % 12
        if add_month == 0:
            add_month = 12
            add_years -= 1
        new_date = f'{year + add_years}_m{add_month:02d}'

    return new_date


def table_path(table: str, config_file: str) -> str:
    """
    Get the path to the table file. First the cwd is searched and if the file is not found the path relative to the
    configuration file is tested.

    Parameters
    ----------
    table:

    config_file:

    Returns
    -------
        Path to the table file
    """
    if os.path.isfile(table):
        table = table
    else:
        table = os.path.join(os.path.dirname(os.path.abspath(config_file)), table)
    return table


def read_table(filename: str):

    """
    Load the text table into a dictionary for parsing by the ensemble code

    Parameters
    ----------
    filename :
        path of the run configuration table
    """
    import pandas as pd
    data = pd.read_csv(filename, delim_whitespace=True, comment='#')

    data_dict = {}
    for key in data.keys():
        if ':' in key:
            # this column is for a nested dictionary
            #   note: this approach could be generalized for arbitrary nesting if desired
            nested_keys = key.split(':')

            # add up to two levels of nested dictionaries
            if nested_keys[0] not in data_dict:
                data_dict[nested_keys[0]] = {}
            if len(nested_keys) > 2 and nested_keys[1] not in data_dict[nested_keys[0]]:
                data_dict[nested_keys[0]][nested_keys[1]] = {}
            if len(nested_keys) > 3:
                raise Exception("Unsupported run table key {}".format(key))

            # populated nested dictionaries
            if len(nested_keys) == 2:
                data_dict[nested_keys[0]][nested_keys[1]] = [x for x in data[key].values]
            elif len(nested_keys) == 3:
                data_dict[nested_keys[0]][nested_keys[1]][nested_keys[2]] = [x for x in data[key].values]
        else:
            data_dict[key] = [x for x in data[key].values]  # convert to list from array

    return data_dict


def write_table(data: dict, filename: str):

    import pandas as pd

    table_dict = {}
    for key in data.keys():

        # flatten the dictionary
        if type(data[key]) is dict:
            for dkey in data[key].keys():
                table_dict[f'{key}:{dkey}'] = data[key][dkey]
        else:
            table_dict[key] = data[key]

    pd.DataFrame(data=table_dict).to_csv(filename, sep='\t', index=False)


def is_null(value):

    if (value is None):
        return True

    if type(value) is str:
        if value.lower() in ['none', 'null', 'nil']:
            return True

    return False
