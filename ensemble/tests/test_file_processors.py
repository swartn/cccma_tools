from canesm.util import ProcessBash, ProcessNamelist
import textwrap
import pytest


class TestProcess:

    def test_basic_namelist(self):
        code = " abc = 1     ! abc = 1\n"
        line = ProcessNamelist(code).process({'abc': 2})
        assert line == "      abc=2 ! abc = 1\n"

        code = "      sgbot=0.995               ! bottom full levels (0.995~995hPa)\n" \
               "      shbot=0.995               ! bottom half levels (0.995~995hPa)\n"
        line = ProcessNamelist(code).process({'sgbot': 1.01})
        assert line == "      sgbot=1.01 ! bottom full levels (0.995~995hPa)\n      shbot=0.995               ! bottom half levels (0.995~995hPa)\n"

        code = "      sgdampt_p=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)"
        line = ProcessNamelist(code).process({'sgdampt_p': 0.020})
        assert line == "      sgdampt_p=0.02 ! reduction factor from sgfull to sgtop (0.01=1%)"

        code = "&vtau_scale\n" \
               "      !--- VTAU scale parameters: VOL_scaled = scale * VOL\n" \
               "      vtau_scale_use=0      ! turn on scaling\n" \
               "      vtau_lwe_scale=$vtau_scale\n" \
               "      vtau_swe_scale=$vtau_scale"
        line = ProcessNamelist(code).process({'vtau_scale_use': 1})
        assert line == "&vtau_scale\n" \
                       "      !--- VTAU scale parameters: VOL_scaled = scale * VOL\n" \
                       "      vtau_scale_use=1 ! turn on scaling\n" \
                       "      vtau_lwe_scale=$vtau_scale\n" \
                       "      vtau_swe_scale=$vtau_scale"


    def test_unknown_namelist_param(self):

        code = '\nvtau_scale_use=1 ! test unknown key'
        with pytest.raises(ValueError):
            ProcessNamelist(code).process({'abd': 2})



    def test_basic_code(self):

        code = textwrap.dedent("""
        ###################
        #    test code
        ###################
        
        abc = 1     # comment here
        bcd=1
        
        my_abc=1
         my_bcd = 1
         
         efg =1
         #def=1    
         
        fgh = 1   
        
        hij=1
        """)

        settings = {'abc': 2, 'bcd': 2, 'efg': 2, 'fgh': 2}
        code_out = textwrap.dedent("""
        ###################
        #    test code
        ###################

        abc=2 # comment here
        bcd=2

        my_abc=1
         my_bcd = 1

        efg=2
         #def=1    

        fgh=2

        hij=1
        """)

        line = ProcessBash(code).process(settings)
        assert line == code_out

        code = " abc = 1     # comment here abc = 1\n"
        line = ProcessBash(code).process({'abc': 2})
        assert line == "abc=2 # comment here abc = 1\n"

        code = "#def=5"
        with pytest.raises(ValueError):
            ProcessBash(code).process({'def': 6})


    def test_unknown_bash_param(self):

        code = '#\nabc=1'
        with pytest.raises(ValueError):
            ProcessBash(code).process({'abd': 2})

    def test_multivar_line(self):

        code = "#\nabc=1; bcd=2\n"
        line = ProcessBash(code).process({'abc': 2})
        assert line == "#\nabc=2;bcd=2\n"

        code = "#\nabc=1; bcd=2\n"
        line = ProcessBash(code).process({'bcd': 1})
        assert line == "#\nabc=1;bcd=1\n"

        code = " abc=1 ; bcd =  2;"
        line = ProcessBash(code).process({'abc': 2, 'bcd': 4})
        assert line == "abc=2;bcd=4"

        code = " abc=1 ; bcd =  2; efg=4"
        line = ProcessBash(code).process({'abc': 2, 'bcd': 4})
        assert line == "abc=2;bcd=4;efg=4"

        code = " abc=1 ; bcd =  2; efg=   4"
        line = ProcessBash(code).process({'abc': 2, 'bcd': 4, 'efg': 5})
        assert line == "abc=2;bcd=4;efg=5"

        code = " abc=1 ; bcd =  2; efg=   4 # comment! sldkfj = 4"
        line = ProcessBash(code).process({'abc': 2, 'bcd': 4, 'efg': 5})
        assert line == "abc=2;bcd=4;efg=5 # comment! sldkfj = 4"

        code = " abc=1; \n"
        with pytest.raises(ValueError):
            line = ProcessBash(code).process({'abc': 2, 'bcd': 4})

    def test_variable_sub(self):

        code = '#\nabc=1\n varname=$ghg_scale\nvarname2=$ghg_scale\nvarname5=ghg_scale'
        line = ProcessBash(code).process({'$ghg_scale': 8})
        assert line == '#\nabc=1\n varname=8\nvarname2=8\nvarname5=ghg_scale'

        code = '#\nabc=1\n varname=$ghg_scale     # comment here\nvarname2=$ghg_scale\nvarname5=ghg_scale'
        line = ProcessBash(code).process({'$ghg_scale': 8})
        assert line == '#\nabc=1\n varname=8     # comment here\nvarname2=8\nvarname5=ghg_scale'

        code = '#\nabc=1\n varname=$ghg_scale \nvarname2=$ghg_scale\nvarname5=ghg_scale'
        with pytest.raises(ValueError):
            ProcessBash(code).process({'$gh_scale': 8})
