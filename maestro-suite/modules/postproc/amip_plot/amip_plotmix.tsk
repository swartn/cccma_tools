#!/bin/bash

printf "\n=====>  AGCM_Plotmix starts: $(date) ###########\n\n"

###############################################################
#Objective: multi-year ploting with plotmix
#
#
set -e

  pool_uxxx=pc
  run=${runid}
  USER=${USER}
  JOBNAME=${runid}

  m01="  012";  d="B";
  kax="    1"; kin="    1"; lxp="    0"; map="    0"; b="  +";

  lrt="   63"; lmt="   63"; typ="    2";
  lon="  128"; lat="   64"; npg="    3";
  ncx="    2"; ncy="    2";
  resol=128_64;

#  * ............................ Condef Parameters ............................

  nocldo=off
  stat2nd=off
  datatype=specsig
  blckcld=on
  debug=off
  obsdat=on
  colourplots=on
  shade=off
  plunit="vic";

  range=${pool_start_year}"m"${pool_start_month}"_"${run_stop_year}"m"${pool_stop_month}
  for days in MAM JJA SON DJF ; do
    obsday=$days
    season=$(echo "$days" | tr '[:upper:]' '[:lower:]')
    plpfn="${run}_${range}_${season}_plotmix";
    flabel="${pool_uxxx}_${runid}_${range}_${season}_"

    erafile=pd_era_int_200309_200808_${season}_;
    obspcp_cmap=pd_cmap_month_200309_200808_144x72_pcpn_${season};
    obspcp_gpcp=pd_gpcp2.3_month_200309_200808_144x72_pcp_${season};
    obsceres=pd_ceres_ebaf_toa_v2_7_200309-200808_grid_${season};
    obsceres_msk=pd_ceres_ebaf_toa_v2_7_200309-200808_grid_tmask_${season};
    ceres_cld=pd_ceres_syn1deg_terra_ed2_5_subset_200309_200808_grid_${season};
    obsisccp=pd_isccp_d2_200309_200808_cloud_data_${season};
    obsuwisc=pd_uwisc_200309_200808_lwp_${season};
    obsuwisc_msk=pd_uwisc_200309_200808_lwp_tmask_${season};
    obsssmi=pd_ssmi_1987_2007_${season};
    obsssmi_msk=pd_ssmi_1987_2007_tmask_${season};
    warren_cld=pd_warren_cld_73_37;
    obssnow=pd_blended_mean4_gl_200309_200808_gp_sno_${season};
    mask=gcm3.5_landmask_128x64v5;
#
    join=3
    source plotmix16.sh
  done

  gpcp=on
  for days in MAM JJA SON DJF ANN ; do
    obsday=$days
    season=$(echo "$days" | tr '[:upper:]' '[:lower:]')
    plpfn="${run}_${range}_${season}_anomaly";
    flabel="${pool_uxxx}_${runid}_${range}_${season}_"

    if [[ "$season" == "ann" ]] ; then
       model1="pd_era_int_2004_2008_ann_"
    else
       model1="pd_era_int_200309_200808_${season}_"
    fi
    model2="${pool_uxxx}_${runid}_${range}_${season}_"

    obspcp_cmap=pd_cmap_month_200309_200808_144x72_pcpn_${season};
    obspcp_gpcp=pd_gpcp2.3_month_200309_200808_144x72_pcp_${season};
    obsceres=pd_ceres_ebaf_toa_v2_7_200309-200808_grid_${season};
    obsceres_msk=pd_ceres_ebaf_toa_v2_7_200309-200808_grid_tmask_${season};
    ceres_cld=pd_ceres_syn1deg_terra_ed2_5_subset_200309_200808_grid_${season};
    obsuwisc=pd_uwisc_200309_200808_lwp_${season};
    obsuwisc_msk=pd_uwisc_200309_200808_lwp_tmask_${season};
    obssnow=pd_blended_mean4_gl_200309_200808_gp_sno_${season};

    source anomaly15.sh
  done

#  *      Implied heat transport calculations

  model="${pool_uxxx}_${runid}_${range}_";

  obs_ceres=pd_ceres_ebaf_toa_v2_7_200309-200808_grid_;
  obs_era="pd_era_int_200309_200808_";
#
  day1="WINTER";  day2="SPRING"; day3="SUMMER"; day4="FALL"; days="ANN";

  ocean_mask_file="uwl_mhbasin_128_64";
  land_mask="new_mask_128_64";
  shades1="              7  186  183  180  140  105  103  101";
  values1="               -100.      -50.      -0.1      +.01       50.      100.";
  kax="    1"; kin="    1"; lxp="    0"; map="    0"; b="  +";

  plpfn="${run}_${range}_heatrans";

  #  * ............................ Condef Parameters ............................
  noggsave=off

  source heatrans16.sh

  printf "\n=====>  AGCM Plotmix ends: $(date) ###########\n\n"
