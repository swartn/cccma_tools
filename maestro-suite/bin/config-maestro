#!/usr/bin/env python
#   TODO: does it make sense to use a Class for the run's sequencer? This needs to be done for imsi anyway
import argparse
import xml.etree.ElementTree as ET
import re
import shutil
import copy
from pathlib import Path

def config_maestro(sequencer_directory: str, config_settings: dict):
    """
        Main configuration driver for maestro
    """
    # define sequencer directory file structure
    sequencer_file_structure = {
        "root_directory"        : Path(sequencer_directory),
        "defaults_directory"    : Path(sequencer_directory).joinpath("defaults"),
        "module_directory"      : Path(sequencer_directory).joinpath("modules"),
        "resource_directory"    : Path(sequencer_directory).joinpath("resources")
        }

    set_resources(sequencer_file_structure, config_settings)
    set_experiment_flow(sequencer_file_structure['module_directory'], sequencer_file_structure['defaults_directory'],
                            config_settings)
    set_expoptions_file(sequencer_file_structure['root_directory'], sequencer_file_structure['defaults_directory'],
                            config_settings)

def set_resources(sequencer_file_structure: dict, config_settings: dict):
    """
        Extract necessary maestro resources files and make necessary modifications for the
        desired run.
    """
    populate_run_resource_directory(sequencer_file_structure["resource_directory"],
                                    sequencer_file_structure["module_directory"],
                                    sequencer_file_structure["defaults_directory"])
    config_resource_files(sequencer_file_structure["resource_directory"], config_settings)

def populate_run_resource_directory( resource_directory : Path, module_directory : Path,
                                        defaults_directory : Path):
    """
        Populate the run specific resource directory (i.e. sequencer_directory/resources)
        by simplying copying the default tree under defaults_directory/resources.

        These files can then be modified uniquely for each run.
    """
    default_resource_directory = defaults_directory.joinpath("resources")
    if not resource_directory.is_dir():
        shutil.copytree(default_resource_directory, resource_directory)
    else:
        print("resource directory already populated")

def config_resource_files(resource_directory: Path, config_settings: dict):
    """
        Set run specific settings in the resource files.
    """
    default_resource_file = resource_directory.joinpath("resources.def")
    config_default_resource_file(default_resource_file, config_settings)
    config_task_resource_files(resource_directory, config_settings)

def config_default_resource_file(default_resource_file: Path, config_settings: dict):
    """
        Propogate the run's configuration settings into the default resource file
    """
    set_variable_in_shell_file('BACKEND', config_settings['BACKEND'].strip('"'), default_resource_file)
    set_variable_in_shell_file('FRONTEND', config_settings['FRONTEND'].strip('"'), default_resource_file)
    set_variable_in_shell_file('SEQ_JOBNAME_PREFIX', f"{config_settings['RUNID']}-".strip('"'), default_resource_file)
    set_variable_in_shell_file('NUMBER_OF_MODEL_LOOPS', config_settings['number_of_model_chunks'].strip('"'), default_resource_file)
    set_variable_in_shell_file('NUMBER_OF_POSTPROC_LOOPS', config_settings['number_of_postproc_chunks'].strip('"'), default_resource_file)

def config_task_resource_files(resource_directory: Path, config_settings: dict):
    """
        For each task specific resource file (i.e. xml files underneath 'resources' that aren't container.xml)
        check for an entry in 'config_settings' with TASK_NAME as the key, which contains a nested dictionary
        with the desired resources. Where the supported resource keys are

            memory      : defines the amount of memory needed for the job
            processors  : defines the number of desired processors for the job
            wallclock   : defines the wallclock time, in minutes, for the job

        If this dictionary is found - apply the desired resource settings in the maestro task files
    """
    # define the supported configurable variables and how they map to
    #   fields in the resource files
    configurable_vars_map = { "memory"      : "memory",
                              "processors"  : "cpu",
                              "wallclock"   : "wallclock"}

    for task_resource_file in resource_directory.glob('**/*.xml'):
        task_name = task_resource_file.stem
        if task_name == "container": continue # skip container.xml files

        # try to get the resource request (will return None if not present)
        task_resource_request = config_settings.get(task_name, None)
        if task_resource_request:

            # load the batch specs from the current resource file
            task_resource_contents = ET.parse(task_resource_file)
            task_resource_specs = task_resource_contents.getroot()
            if len(task_resource_specs.findall('BATCH')) != 1:
                err_string = f"Failed to find a unique BATCH specification section in {str(task_resource_file)}!!"
                err_string += " Task resource files must contain ONE BATCH section!"
                raise Exception(err_string)
            batch_specs = task_resource_specs.find('BATCH')

            # set the desired options
            for resource_key, resource_val in task_resource_request.items():
                try:
                    maestro_resource_key = configurable_vars_map[resource_key]
                except KeyError:
                    raise KeyError(f"Unsupported resource specification --> {resource_key} <--")
                batch_specs.set(maestro_resource_key, str(resource_val))
            task_resource_contents.write(task_resource_file)

def set_variable_in_shell_file(variable_name : str, variable_value : str, shell_file: Path):
    """
        Set desired variable definition in a file that can be read by a shell.
    """
    with open(shell_file, "r+") as opened_shell_file:
        file_contents = opened_shell_file.read()
        look_behind_regex_pattern = f'(?<={variable_name})\s*=\s*.*'
        updated_contents = re.sub(look_behind_regex_pattern, f'={variable_value}', file_contents)
        opened_shell_file.seek(0)
        opened_shell_file.truncate()
        opened_shell_file.write(updated_contents)

def get_config_settings(config_directory: str, root_config_file: str = "canesm-shell-params.sh") -> dict:
    """
        Load in the necessary configuration parameters for maestro, from the given
        configuration directory.

        config_directory: location of configuration files
        root_config_file: file containing the high level settings, which might control the need
                            for downstream config files

        Note: this function was built as a quick tool to load in shell based config files
            and place them in a dictionary. It is expected that this will be replaced by
            imsi in the near future. However, if the necessary config settings expand much more
            this should be cleaned up to better fit the DRY principle (don't repeat yourself)
    """
    config_settings = {}

    # load root configuration settings
    path_to_root_config_file = Path(config_directory).joinpath(root_config_file)
    with open(path_to_root_config_file, "r") as opened_config_file:
        for line in opened_config_file:
            key, value = line.strip().split("=",1)
            config_settings[key] = value

    # load any downstream config settings
    ## compute system settings
    if "compute_system" in config_settings:
        compute_system = config_settings["compute_system"]
        compute_system_config_file = f"{compute_system}.compute_system.cfg"
        path_to_compute_system_file = Path(config_directory).joinpath(compute_system_config_file)
        with open(path_to_compute_system_file, "r") as opened_config_file:
            for line in opened_config_file:
                key, value = line.strip().split("=")
                if key in config_settings:
                    raise KeyError(f"{key} found in multiple configuration files!")
                config_settings[key] = value

    # translate compute resource resource request to maestro resource specs
    translate_resource_requests(config_settings)
    return config_settings

def translate_resource_requests(config_settings : dict):
    """
        Ingest the run's configuration settings and translate the existing
        resource configuration parameters and translate them to settings used
        by maestro resource files.

        Note:
            - this is currently meant as a translation between existing
                canesm.cfg parameters to those need by
                'config_task_resource_files'. If at a later point these resources
                are defined directly in the configuration files, this shouldn't
                be needed.
            - the translation functions return 'None' if the given key isn't found
    """
    def _set_memory( key : str, cfg_settings : dict):
        """
            Translate
                '*mb', '*Mb', '*MB', '*mB' -> 'M'
                '*gb', '*Gb', '*GB', '*gB' -> 'G'
            or keep 'G'/'M'.
        """
        expected_format  = r'[0-9]+[a-zA-Z]{1,2}\b' # any number of integers and a two letter memory unit
        memory_unit_patt = r'[a-zA-Z]{1,2}'
        memory_unit_map  = { "GB" : "G",
                             "G"  : "G",
                             "MB" : "M",
                             "M"  : "M" }

        # try to access the memory settings key
        original_memory_request = cfg_settings.get(key,None)
        updated_memory_request = None
        if original_memory_request:
            if not re.match(expected_format, original_memory_request):
                raise Exception(f"Unsupported memory format! {key} = {original_memory_request}")
            original_memory_unit_id = re.search(memory_unit_patt, original_memory_request).group(0)
            updated_memory_unit_id  = memory_unit_map.get(original_memory_unit_id.upper(), None)
            if updated_memory_unit_id:
                updated_memory_request  = re.sub(original_memory_unit_id, updated_memory_unit_id, original_memory_request)
            else:
                raise Exception(f"Unsupported memory unit! Must use one of GB,gb,Gb,G,g,MB,mb,Mb,M,m!")
        return updated_memory_request

    def _set_wallclock( key : str, cfg_settings : dict):
        """
            Translate wallclock times given in seconds to minutes
        """
        original_wallclock_request = int(cfg_settings.get(key,None))
        updated_wallclock_request = None
        if original_wallclock_request:
            updated_wallclock_request = original_wallclock_request // 60
            remainder = original_wallclock_request % 60
            if remainder != 0:
                raise ValueError("wallclock specifications must be divide evenly into minutes!")
        return updated_wallclock_request

    def _set_processors( key : str, cfg_settings : dict):
        """
            This currently determines the processor count by assuming the given
            number of the number of nodes, and then using the compute system configuration
            to set the appropriate number of processors.

            This is based off the use of 'phys_node' and is way too specific. There is
            also no intelligent way to know that nodes are requests, so they should definitely be
            reconsidered as we move a way from old configuration variables.
        """
        original_node_request = cfg_settings.get(key,None)
        updated_processor_request = None
        if original_node_request:
            original_node_request = int(original_node_request)
            processors_per_node = int(cfg_settings['BACKEND_PROC_PER_NODE'])
            updated_processor_request = original_node_request*processors_per_node
        return updated_processor_request

    # note that the way the model_run procs gets set will need to be updated when we no longer
    #   use a request node count
    resource_request_mods = {
        "model_run" : {
            "processors" : _set_processors('phys_node', config_settings),
            "memory"     : _set_memory('gcmmem', config_settings),
            "wallclock"  : _set_wallclock('gcmtime', config_settings)
        },
        "agcm_diag" : {
            "memory" : _set_memory('memory_agcm_diag', config_settings)
        },
        "agcm_runtime_diag" : {
            "memory" : _set_memory('memory_agcm_rtd', config_settings)
        }}

    # populate fields in config_settings, if modifications are actually requested
    for task_name in resource_request_mods:
        task_modifications = { key : val for key, val in resource_request_mods[task_name].items() if val is not None }
        if task_modifications:
            config_settings[task_name] = task_modifications

def set_expoptions_file(sequencer_root_directory : Path, defaults_directory : Path,
                            config_settings: dict, expoptions_filename : str = "ExpOptions.xml"):
    """
        Set the ExpOptions file that is used to control settings for the 'xflow' and 'xflow_overview'
        file.

        This is NOT a generic function that sets "experiment options".
    """
    default_expoptions_file = defaults_directory.joinpath(expoptions_filename)
    run_expoptions_file     = sequencer_root_directory.joinpath(expoptions_filename)

    # populate run file if necessary
    if not run_expoptions_file.is_file():
        shutil.copy(default_expoptions_file, run_expoptions_file)

    # make necessary modifications
    expoptions_content = ET.parse(run_expoptions_file)
    expoptions = expoptions_content.getroot()
    expoptions.set("displayName", config_settings['RUNID'])
    expoptions.set("shortName", config_settings['RUNID'])
    expoptions_content.write(run_expoptions_file)

def set_experiment_flow(module_directory: Path, defaults_directory: Path, config_settings: dict):
    """
        First populate, if necessary, the run's flow files, under the module directory, using
        the default files in the defaults directory. Then alter the necessary flow.xml files
        according to the configuration settings.

        As of now, this function only has the ability to trim task nodes from the flow files. If we want to
        add more functionality, i.e. the ability to remove full families/modules, we will need to extend/add
        trim functions.
    """
    # populate run's flow files
    flow_file_basename = "flow.xml"
    module_flow_files  = {}
    modules = [ module.name for module in module_directory.iterdir() if module.is_dir() ]
    default_flow_file_directory = defaults_directory.joinpath("flow_definitions")
    for module in modules:
        default_flow_file_name = f"{module}_{flow_file_basename}"
        default_flow_file      = default_flow_file_directory.joinpath(default_flow_file_name)
        run_flow_file          = module_directory.joinpath(module).joinpath(flow_file_basename)
        if not run_flow_file.is_file():
            shutil.copy(default_flow_file, run_flow_file)
        module_flow_files[module] = run_flow_file

    # configure the necessary flow files
    if config_settings['config'] == "AMIP":
        # remove ESM ocean, and OMIP nodes
        trim_nodes(".*ocean.*", module_flow_files['postproc'])
        trim_nodes(".*omip.*", module_flow_files['postproc'])
    elif config_settings['config'] == "OMIP":
        # remove ESM agcm, and AMIP nodes
        trim_nodes(".*agcm.*", module_flow_files['postproc'])
        trim_nodes(".*amip.*", module_flow_files['postproc'])
    elif config_settings['config'] == "ESM":
        # remove AMIP and OMIP nodes
        trim_nodes(".*amip.*", module_flow_files['postproc'])
        trim_nodes(".*omip.*", module_flow_files['postproc'])
    if config_settings['number_of_model_chunks'] != config_settings['number_of_postproc_chunks']:
        desired_dependency_relationship = int(config_settings['number_of_model_chunks'])/int(config_settings['number_of_postproc_chunks'])
        update_loop_dependencies(loop_name = 'rebuild_loop', dependency_target = "model_loop",
                                 flow_file = module_flow_files['postproc'],
                                 dependency_relationship = desired_dependency_relationship,
                                 number_of_iterations = int(config_settings['number_of_postproc_chunks']))
        update_loop_dependencies(loop_name = 'diagnostics_loop', dependency_target = "model_loop",
                                 flow_file = module_flow_files['postproc'],
                                 dependency_relationship = desired_dependency_relationship,
                                 number_of_iterations = int(config_settings['number_of_postproc_chunks']))
        update_loop_dependencies(loop_name = 'wrap_up_loop', dependency_target = "model_loop",
                                 flow_file = module_flow_files['postproc'],
                                 dependency_relationship = desired_dependency_relationship,
                                 number_of_iterations = int(config_settings['number_of_postproc_chunks']))

def update_loop_dependencies(loop_name : str, dependency_target : str, flow_file : Path,
                             dependency_relationship : int, number_of_iterations : int,
                             starting_iteration : int = 1):
    """
        For the given 'loop_name', within the given 'flow_file', update its dependency
        definition for the given 'dependency' such that iteration N of it only runs
        once iteration c*N has ran for the given dependency, where c is given
        by 'dependency_relationship', which must be an integer greater than 1.

        Notes:
            - as of the initial writing of this function, maestro currently doesn't support
                a dependency relationship function, beyond 1:1 (meaning that iteration N
                of the loop won't run unless iteration N of the dependency has completed).
                As a result, we must _explicitly_ lay out the dependencies for _each_ iteration.
                This leads to gross dependency definitions, but c'est la vie. Nevertheless,
                this is why we need 'starting_iteration' and 'number_of_iterations'.

                A feature request has been submitted to maestro in order to make this cleaner
                    - https://gitlab.science.gc.ca/CMOI/maestro/issues/423
                If this is implemented, this code can be significantly cleaned up
    """
    flow = ET.parse(flow_file)
    module_definition = flow.getroot()

    # find the loop node
    target_loop_node = None
    for loop_node in module_definition.findall("LOOP"):
        node_name = loop_node.get("name")
        if node_name == loop_name:
            if target_loop_node is not None:
                raise Exception(f"Multiple loops with name {loop_name} exist in {flow_file.name}!")
            target_loop_node = loop_node
    if target_loop_node is None:
        raise Exception(f"Failed to find a loop with the name {loop_name} in {flow_file.name}!")

    # clear the existing DEPENDS_ON tags for the given dependency and
    #   populate information various text information that will be used to format the updated entries
    index_name          = None
    local_index_name    = None
    tail_string         = None          # will define the text between successive tags (i.e. new line/tag characters)
    path_to_dependency_target = None
    dependency_definitions = list(target_loop_node.findall("DEPENDS_ON"))
    for dep_def in dependency_definitions:
        path_to_dependency = dep_def.get("dep_name")
        dep_name = Path(path_to_dependency).name
        if dep_name == dependency_target:
            if index_name is None:
                index_def = dep_def.get("index")
                index_name = re.sub(r'=.*','',index_def)
            if local_index_name is None:
                local_index_def = dep_def.get("local_index")
                local_index_name = re.sub(r'=.*','',local_index_def)
            if path_to_dependency_target is None:
                path_to_dependency_target = path_to_dependency
            if tail_string is None:
                tail_string = dep_def.tail
            target_loop_node.remove(dep_def)

    # populate the new dependencies
    for i in range(starting_iteration, number_of_iterations+1):
        # create index/local index definitions
        target_index    = int(dependency_relationship*i)
        index_def       = f"{index_name}={target_index:d}"
        local_index_def = f"{local_index_name}={i:d}"

        # create xml element
        new_dependency = ET.Element('DEPENDS_ON')
        new_dependency.set("dep_name", path_to_dependency_target)
        new_dependency.set("index", index_def)
        new_dependency.set("local_index", local_index_def)
        new_dependency.tail = tail_string

        # add it to the loop node!
        target_loop_node.insert(0,new_dependency)

    # write the updated flow file
    flow.write(flow_file)

def trim_nodes(pattern : str, flow_file : Path):
    """
        Given a pattern, find all matching tasks/family nodes from
        the given flow file and removes them along with associated submits/depends
        on tags.

        TODOS:
            - assess if a pattern is really the best way to do this.

        Notes:
            - DEPENDS_ON tags have pathnames for names, which is why we use basename,
                as we don't want to match against the path
            - we don't remove the node in the first loop, as we want to avoid altering
                the object we are iterating over
            - see https://docs.python.org/3/library/xml.etree.elementtree.html#xpath-support
                for information on "XPATH" searching
            - we should never find more than 1 matching parent node for each node to trim,
                but the check is present to make sure our assumptions are correct
            - we build list of nodes to remove in sublists depending on tags so we can make
                sure we remove the DEPENDS_ON and SUBMIT nodes first, and then the TASKS and FAMILY
                nodes, as the latter two can contain the former - and FAMILY nodes can contain TASKS
    """
    flow = ET.parse(flow_file)
    module_definition = flow.getroot()

    # define the map between tag types and their "name" attribute
    node_tag2name_map = { "FAMILY"      : "name",
                          "TASK"        : "name",
                          "SUBMITS"     : "sub_name",
                          "DEPENDS_ON"  : "dep_name" }

    nodes_to_remove = { "FAMILY"        : [],
                        "TASK"          : [],
                        "SUBMITS"       : [],
                        "DEPENDS_ON"    : []}

    # identify nodes to remove
    for node in module_definition.iter():
        tag_name_attribute = node_tag2name_map.get(node.tag, None)
        if tag_name_attribute:
            node_name = node.get(tag_name_attribute)
            node_basename = Path(node_name).name
            if re.match(pattern, node_basename):
                nodes_to_remove[node.tag].append(node)

    # remove nodes in desired order if any were found
    ordered_nodes_to_remove = nodes_to_remove["DEPENDS_ON"] + nodes_to_remove["SUBMITS"]\
                                    + nodes_to_remove["TASK"] + nodes_to_remove["FAMILY"]
    if ordered_nodes_to_remove:
        for node in ordered_nodes_to_remove:
            # find the parent node for the desired node
            tag_name_attribute = node_tag2name_map.get(node.tag, None)
            node_name = node.get(tag_name_attribute)
            xpath_search_pattern = f".//*[@{tag_name_attribute}='{node_name}']/.."
            parent_nodes = module_definition.findall(xpath_search_pattern)
            if len(parent_nodes) != 1:
                raise Exception("Failed to find unique parent node for desired leaf node!")
            else:
                parent_node = parent_nodes[0]

            # trim!
            parent_node.remove(node)

        # write updated file
        flow.write(flow_file)
    else:
        print(f"no nodes matching {pattern} within {flow_file}")

def parse_args():
    cli = argparse.ArgumentParser(description="configure the maestro sequencer")
    cli.add_argument("sequencer_dir", action = "store",
                        help = "location of maestro files")
    cli.add_argument("config_dir", action = "store",
                        help = "location of run configuration directory")
    args = vars(cli.parse_args())
    return args

if __name__ == '__main__':
    args = parse_args()
    config_settings = get_config_settings(config_directory = args['config_dir'])
    config_maestro(sequencer_directory = args['sequencer_dir'],
                    config_settings = config_settings)

