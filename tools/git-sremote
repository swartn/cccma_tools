#!/bin/bash
# 
# Add a remote to CanESM repo (and the associated submodules)
#   - First arg:                namespace that the CanESM remote belongs to
#   - (optional) Second Arg:    name to attach to the new remote - if not given, defaults to the first argument


#==================
# handle arguments
#==================
# flags
ROOT_ADDRESS=""
SUPERREPO_NAME=""
DEFAULT_ROOT_ADDRESS="git@gitlab.science.gc.ca"
DEFAULT_SUPERREPO_NAME="CanESM5"
while getopts r:s: flag; do
    case $flag in
        r) ROOT_ADDRESS="$OPTARG"   ;;
        s) SUPERREPO_NAME="$OPTARG" ;;
        ?) echo "Invalid flag --> $flag <--"; 
           exit 1 ;;
    esac
done
if [[ -z $ROOT_ADDRESS ]]; then
    ROOT_ADDRESS=$DEFAULT_ROOT_ADDRESS
fi
if [[ -z $SUPERREPO_NAME ]]; then
    SUPERREPO_NAME=$DEFAULT_SUPERREPO_NAME
fi
shift $(( OPTIND - 1 ))

# positional args
[[ -z "$1" ]] && { echo "ERROR: You provide the username as the first argument" ; exit 1 ; }
name_space=$1
if [[ -n "$2" ]] ; then
    # if second argument given, set the remote name to it
    remote_name=$2
else
    # default to using the 1st argument
    remote_name=$1
fi

#=====================================
# Verify that we are in the super-repo
#=====================================
[ -d CanAM ] ||  { echo "ERROR: You MUST be in the CanESM super-repo" ; exit 1 ; }

#=================
# Add the remotes!
#=================
git submodule foreach --recursive "git remote add ${remote_name} ${ROOT_ADDRESS}:${name_space}/\$(basename \$name).git"
git remote add ${remote_name} ${ROOT_ADDRESS}:${name_space}/${SUPERREPO_NAME}.git
